var gulp, sass, sourcepamps;

gulp = require('gulp');
gutil = require('gulp-util');
sass = require('gulp-sass');
sourcemaps = require('gulp-sourcemaps');
autoprefixer = require('gulp-autoprefixer');
cssBase64 = require('gulp-css-base64');

gulp.task('default', ['compile-scss']);
gulp.task('compile-scss', function() {
  gulp.src('*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({ indentedSyntax: false, errLogToConsole: true }))
  .pipe(cssBase64({ maxWeightResource: 2000 }))
  .pipe(autoprefixer())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('.')).
  on('error', gutil.log);
});
gulp.task('watch-scss', function() {
  gulp.watch('*.scss', ['compile-scss']);
});
gulp.task('watch', ['watch-scss']);
