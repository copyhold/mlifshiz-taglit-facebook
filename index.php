<?php
if ($_GET['thankyou'] && !$_POST) die('what!');
if ($_POST) {
  $ch = curl_init("https://profiles.israelexperts.com/PTL/tytytytytytytytyt2.php");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, ['email'=>$_POST['email'], 'fullname' => $_POST['name'], 'phone' => $_POST['tel']]);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_exec($ch);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width">
<meta charset="UTF-8">
  <title></title>
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="style.css">
</head>
<body class="<?php echo ($_GET['thankyou'] ? 'thankyou' : 'regular') ; ?>">
<header>
<a class="logo-taglit" href="taglit-site"><img src="logo-taglit.svg" alt=""></a>
<div class="ie">
  <a class="logo-ie" href="https://israelexperts.co.il"><img src="logo-ie.svg" alt=""></a>
  <a class="twitter" href="https://twitter.com/search?q=%23israelexperts&src=typd"><img src="logo-ie-blue.png" alt=""></a>
</div>
<div class="so">
  <a class=yo target=_blank href="https://www.youtube.com/user/israelexpertstaglit">youtube</a>
  <a class=in target=_blank href="http://instagram.com/israelexperts">instagram</a>
  <a class=tw target=_blank href="https://twitter.com/TaglitIsraelExp">twitter</a>
  <a class=fb target=_blank href="https://www.facebook.com/TaglitIsraelExperts">facebook</a>
</div>
</header>
<figure>
<?php if ($_GET['thankyou']): ?>
  <img src="thankyou.png">
  <figcaption>thank you text</figcaption>
  <script>setTimeout(function() { location.href = "http://www.absolutelyisrael.com/" }, 4000); </script>
<?php else: ?>
  <img src="beyong.png" alt="">
  <figcaption>Discover Israel: the highs the lows, the weird and the wonderful</figcaption>
<?php endif; ?>
</figure>
<?php if ($_GET['thankyou']): ?>
<img src="thankyou-people.jpg">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1025640144;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "xB6FCNHt-1wQ0I2I6QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1025640144/?label=xB6FCNHt-1wQ0I2I6QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php else: ?>
<ul>
  <li>
    <img src="highs.jpg" alt="">
    <span>highs</span>
  </li>
  <li>
    <img src="lows.jpg" alt="">
    <span>lows</span>
  </li>
  <li>
    <img src="wierd.jpg" alt="">
    <span>wierd</span>
  </li>
  <li>
    <img src="wonderful.jpg" alt="">
    <span>wonderful</span>
  </li>
</ul>
<article>
Take a journey, deeper into Israel’s many layers and dimensions.  Open your mind & heart and be inspired.  Dance through the streets of Tel Aviv and practice yoga under a clear desert sky.  Meet, talk, argue, learn & understand. Embrace people. <span> DISCOVER ISRAEL.</span>
</article>
<div class="banner">
  <p>PRE-REGISTER TODAY 
FOR YOUR 
WINTER 2015/2016 
OR SUMMER 2016 
TRIP OF A LIFETIME!</p>
  <a href="#form">CLICK HERE AND PRE-REGISTER</a>
</div>
<div class=main>
  <div class="form">
    <h2>
      <span>STILL THINKING?</span>
      <span>NEED A LITTLE MORE INFORMATION</span>
      <span>TO MAKE A DECISION? NO PROBLEM!</span>
    </h2>
    <p> Complete your details below and we'll contact you with everything you need to know.     </p>
    <a name=form></a>
    <form action="index.php?thankyou=1" method=post>
      <input required placeholder="Full name" type="text" name=name>
      <input required placeholder="E-mail" type="email" name=email>
      <input required placeholder="Phone" type="tel" name=tel>
      <button type=submit>CONTACT ME</button>
    </form>
  </div>
  <iframe class=video src="https://www.youtube.com/embed/ormzmUZzdZA" frameborder="0" allowfullscreen></iframe>
</div>
<div class="siximages">
  <img src="your-passion.png" alt="">
  <nav>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/discover-israel">
      <img src="discover.jpg" alt="">
      <span>DISCOVER ISRAEL</span>
    </a>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/campus">
      <img src="campus.jpg" alt="">
      <span>CAMPUS CONNECTION</span>
    </a>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/community">
      <img src="community.jpg" alt="">
      <span>COMMUNITY</span>
    </a>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/culinary">
      <img src="culinary.jpg" alt="">
      <span>CULINARY</span>
    </a>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/across-the-universe">
      <img src="across.jpg" alt="">
      <span>ACROSS THE UNIVERSE</span>
    </a>
    <a target=_blank href="http://www.absolutelyisrael.com/trips/entertainment-trip">
      <img src="entertainment.jpg" alt="">
      <span>ENTERTAINMENT TRIP</span>
    </a>
  </nav>
</div>
<footer>
  <img src="taglitwith.png" alt="">
  <p>We create experiences that make an impact.<br>  We encourage you to engage in the beauty and complexity that is Israel.  We show you things and perspectives others won’t.  Our prism is wider and we dig deeper.</p>
  <h3>YOU'LL GET A REAL TASTE. YOU'LL BE TRULY INSPIRED.<br>YOU'LL HAVE AN EXPERIENCE THAT IS UNIQUELY YOURS. </h3>
</footer>
<?php endif; ?>
<div id="fb-root"></div>

<script src="https://connect.facebook.net/en_US/all.js"></script>
<script>
window.fbAsyncInit = function() { 
  FB.init({
    appId : '380427128794821',
    status : true, // check login status
    cookie : true, // enable cookies to allow the server to access the session
    xfbml : true // parse XFBML
  });

  FB.Canvas.setSize(); 
}
function sizeChangeCallback() { FB.Canvas.setSize(); }
</script>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-13204786-7', 'auto');
  ga('send', 'pageview');

</script>
</html>
